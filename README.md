# springboot-actuator

## Prerequisites

* JDK 8

## Installation instructions

**Clone:**

```sh
$ git clone git@github.com:lion01942/springboot-actuator.git
```

**Build:** 

We use [Gradle Wrapper](https://docs.gradle.org/current/userguide/gradle_wrapper.html) for building the application. Execute the following Gradle command from the directory of the parent project:

```sh
$ cd springboot-actuator
$ ./gradlew clean build
```

It will create the Spring Boot executable JAR, `springboot-actuator-0.1.0.jar`, under `build/libs` folder.
 

## How to run the service
```sh
  $ java -jar build/libs/springboot-actuator-0.1.0.jar
```

The health and other metrics of the application can be accessed from:

```
http://localhost:9000/employee
http://localhost:9001/actuator/health
http://localhost:9001/actuator/info
```

## References

**Libraries**
* [Spring Boot](https://spring.io/projects/spring-boot)

## Questions/Comments/Feedbacks/Concerns
[Email](codercheerful@gmail.com)
