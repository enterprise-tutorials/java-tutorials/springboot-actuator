package com.tutorial.actuator;

import lombok.Data;

@Data
public class Employee {

    private final long id;
    private final String name;
}
