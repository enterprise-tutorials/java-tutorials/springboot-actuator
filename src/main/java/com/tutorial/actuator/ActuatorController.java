package com.tutorial.actuator;

import java.util.concurrent.atomic.AtomicLong;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class ActuatorController {

    private static final String template = "Hello, %s!";
    private final AtomicLong counter = new AtomicLong();

    @GetMapping("/employee")
    @ResponseBody
    public Employee sayHello(@RequestParam(name="name", required=false, defaultValue="Stranger") String name) {
        return new Employee(counter.incrementAndGet(), String.format(template, name));
    }

}
